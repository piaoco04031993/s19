alert('Hello')


const num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

const address = [258, 'washington Ave NW', 'California', 90011];

const [streetNumber, streetName, state, zipCode] = address;
console.log(`I live at ${streetNumber} ${streetName}, ${state} ${zipCode}`);

const animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
};

const {name, type, weight, length} = animal;
console.log(` ${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);

let numberArray = [1, 2, 3, 4, 5];

numberArray.forEach((numbers) => {
	console.log(`${numbers}`)
});

const reduceNumber = (previousValue, currentValue) => previousValue + currentValue;
console.log(numberArray.reduce(reduceNumber));

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDogie = new Dog ('Lewk', 1, 'Husky');
console.log(myDogie);