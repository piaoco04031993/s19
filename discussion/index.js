// alert('hi')

// Exponent Operator

	// before
	const secondNum = Math.pow(8, 2);
	console.log(secondNum);

	// ES6 updates
	const firstNum = 8 ** 2;
	console.log(firstNum);

// Template Literals- (`${}`)
	// concatenation operator(+)

// ES6 Updates
	let message = 'Welcome to programming'
	let name = 'John'
// result- Welcome to programming, John

// before the updates
console.log(message + " " + name);

// expected result- Hello John, welcome to programming

// es6 updates
console.log(`Hello ${name}, ${message}`);

/*literals
	[]- array literals

	``- template literals

	${}- placeholder

	{}- object literals 

*/

// Array Destructuring
	// Traditional Array Syntax- let arrayName = []
	// Syntax
		/*
			let [] = arrayName
		*/

//array

const fullName = ['Jonathan', 'Joe', 'Star'];

// accessing array traditionally
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, I am ${fullName[0]} ${fullName[1]} ${fullName[2]}!`);

// array destructuring es6 updates
const [firstName, middleName, lastName] = fullName;

console.log(`Hello, I am ${firstName} ${middleName} ${lastName}!`);

// object destructuring
	/*
		Syntax: 
		let/const {propertyName, propertyName, propertyName} = object;

	*/

const person = {
	givenName: 'Izuku',
	nickName: 'Deku',
	familyName: 'Midoriya'
};

// accessing obj without destructing
console.log(person.givenName);
console.log(person.nickName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} "${person.nickName}" ${person.familyName}! It's good to see you again.`)

// obj destructuring

const {givenName, nickName, familyName} = person;

console.log(`Hello ${givenName} "${nickName}" ${familyName}! It's good to see you again.`)

function getFullName({givenName, nickName, familyName}){
	console.log(`Hello ${givenName} "${nickName}" ${familyName}! It's good to see you again.`)
}
getFullName(person);

// Arrow Functions

// traditional function
	// structure- const functionName = (parameter) => {code block}

/*function hello(parameter){
	console.log('hello world')
}*/

const hello = (parameter) => {
	console.log('Hello World')
}

hello();


const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`)

}

printFullName('Satoru', 'G', 'Gojo');

// arrow function in forEach

let students = ['Megumi', 'Nobara', 'Yuji']

// traditional function
students.forEach(function(student){
	console.log(`${student} is a student`);
});

// arrow function
students.forEach((student) => {
	console.log(`${student} is a student`)
});

// Implicit return statement

// explicit return keyword
/*const add = (x, y) => {
	return x + y
};

let total = add(1, 2);
console.log(total);*/

// implicit return keyword
const add = (x, y) => x + y 

let total = add(1, 2)
console.log(total)

// Default function argument value

const greet = (name = 'User') => {
	return `Good morning ${name}`
}

console.log(greet());

// Class based object
	// creating a class based object
	/*
		Syntax:

		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
	*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// creating an instance

const myCar = new Car();
console.log(myCar);

// inserting values to the obj

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;

console.log(myCar);

// creating an instance from the car class with initialized value

const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);
